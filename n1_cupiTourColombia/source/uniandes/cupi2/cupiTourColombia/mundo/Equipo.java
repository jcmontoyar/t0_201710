/**
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id: InterfazCupiTourColombia.java,v 1.8   Exp $
 * Universidad de los Andes (Bogot� - Colombia)
 * Departamento de Ingenier�a de Sistemas y Computaci�n 
 * Licenciado bajo el esquema Academic Free License version 2.1 
 *
 * Proyecto Cupi2 (http://cupi2.uniandes.edu.co)
 * Ejercicio: n1_cupiTourColombia
 * Autor: Equipo Cupi2
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package uniandes.cupi2.cupiTourColombia.mundo;

/**
 * Clase que representa un equipo participante.
 */
public class Equipo
{
    // -----------------------------------------------------------------
    // Atributos
    // -----------------------------------------------------------------

    /**
     * C�digo UCI del equipo.
     */
    private String codigo;

    /**
     * Nombre del equipo.
     */
    private String nombreEquipo;
    // TODO Parte 2 Punto A: Declarar el atributo nombreEquipo como una cadena de caracteres.

    /**
     * A�o de fundaci�n del equipo.
     */
    // TODO Parte 2 Punto B: Declarar el atributo anioFundacion como un n�mero entero.

    /**
     * Pa�s de donde pertenece el equipo.
     */
    private String pais;

    /**
     * Puntaje alcanzado por el equipo.
     */
    private int puntaje;

    /**
     * Ruta de la imagen donde se encuentra
     */
    private String rutaImagen;
    // -----------------------------------------------------------------
    // M�todos
    // -----------------------------------------------------------------

    /**
     * Inicializa la informaci�n deportiva del equipo. <br>
     * <b>post: </b> El c�digo, el a�o de fundaci�n, el pa�s y el nombre del equipo fueron inicializados con la informaci�n recibida por par�metro. <br>
     * El puntaje fue inicializado en cero. <br>
     * @param pCodigo C�digo UCI del equipo. pCodigo != null && pCodigo != "".
     * @param pFundacion A�o de fundaci�n del equipo. pFundacion != null && pFundacion >= 0.
     * @param pPais Pa�s del equipo. pPais != null && pPais != "".
     * @param pEquipo Nombre del equipo. pEquipo != null && pEquipo != "".
     * @param pRutaImagen Ruta donde se encuentra la imagen del equipo. pRutaImagen != "" && pRutaImagen != null.
     */
    public void inicializar( String pCodigo, int pFundacion, String pPais, String pEquipo, String pRutaImagen )
    {
        // TODO Parte 2 Punto C: Completar el m�todo seg�n la documentaci�n dada.
    }

    /**
     * Retorna el c�digo UCI del equipo.
     * @return C�digo UCI del equipo.
     */
    public String darCodigo( )
    {
        return codigo;
    }

    /**
     * Retorna el nombre del equipo.
     * @return Nombre del equipo.
     */
    public String darNombre( )
    {
    	return nombre;
        // TODO Parte 2 Punto D: Completar el m�todo seg�n la documentaci�n dada.
    }

    /**
     * Retorna el a�o de fundaci�n del equipo.
     * @return A�o de fundaci�n del equipo.
     */
    public int darAnioFundacion( )
    {
        // TODO Parte 2 Punto E: Completar el m�todo seg�n la documentaci�n dada.
    }

    /**
     * Retorna el pa�s del equipo.
     * @return Pa�s del equipo.
     */
    public String darPais( )
    {
        return pais;
    }

    /**
     * Retorna la ruta donde se encuentra la imagen.
     * @return Ruta de la imagen.
     */
    public String darRutaImagen( )
    {
        return rutaImagen;
    }
    /**
     * Retorna el puntaje alcanzado por el equipo.
     * @return Puntaje alcanzado por el equipo.
     */
    public int darPuntaje( )
    {
        return puntaje;
    }

    /**
     * Registra el puntaje ganado en la etapa.<br>
     * <b>post: </b> Se aument� el puntaje con el valor dado por par�metro.<br>
     * @param pPuntaje Nuevo puntaje asignado al equipo. pPuntaje >= 0.<br>
     */
    public void registrarPuntaje( int pPuntaje )
    {
        puntaje = puntaje + pPuntaje;
    }

    /**
     * Registra la penalizaci�n recibida en la etapa.<br>
     * <b>post: </b> Se disminuy� el puntaje en el valor dado por par�metro.<br>
     * @param pPenalizacion Nueva penalizaci�n asignada al equipo. pPenalizacion >= 0.<br>
     */
    public void registrarPenalizacion( int pPenalizacion )
    {
        // TODO Parte 2 Punto F: Completar el m�todo seg�n la documentaci�n dada.
    }

    /**
     * Reinicia la informaci�n (puntaje) del equipo.<br>
     * <b>post: </b> El nuevo puntaje del equipo se cambi� a cero.
     */
    public void reiniciar( )
    {
        // TODO Parte 2 Punto G: Completar el m�todo seg�n la documentaci�n dada.
    }

}
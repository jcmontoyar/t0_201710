/**
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id: InterfazCupiTourColombia.java,v 1.8   Exp $
 * Universidad de los Andes (Bogot� - Colombia)
 * Departamento de Ingenier�a de Sistemas y Computaci�n 
 * Licenciado bajo el esquema Academic Free License version 2.1 
 *
 * Proyecto Cupi2 (http://cupi2.uniandes.edu.co)
 * Ejercicio: n1_cupiTourColombia
 * Autor: Equipo Cupi2
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
package uniandes.cupi2.cupiTourColombia.mundo;

/**
 * Clase que representa el tour de Colombia.
 */
public class CupiTourColombia
{

    // -----------------------------------------------------------------
    // Atributos
    // -----------------------------------------------------------------

    /**
     * Equipo n�mero 1.
     */
    private Equipo equipo1;

    /**
     * Equipo n�mero 2.
     */
    private Equipo equipo2;

    /**
     * Equipo n�mero 3.
     */
    // TODO Parte 3 Punto A: Declarar el atributo equipo3 como un Equipo.

    /**
     * Equipo n�mero 4.
     */
    // TODO Parte 3 Punto B: Declarar el atributo equipo4 como un Equipo.

    /**
     * Etapa en la que se encuentra actualmente el tour. <br>
     * Si el tour se encuentra en la etapa 6, es porque finaliz� el tour.
     */
    private int etapaActual;

    // -----------------------------------------------------------------
    // M�todos
    // -----------------------------------------------------------------

    /**
     * Inicializa la informaci�n deportiva del equipo.<br>
     * <b>post: </b> La etapa se inicializ� en 1.<br>
     * Los cuatro equipos se inicializaron con los siguientes valores.<br>
     * Equipo 1 - C�digo: TNK, A�o de fundaci�n gal�n: 2016, Pa�s: Rusia, Nombre del equipo: Tinkoff, Ruta imagen: tinkoff.png. <br>
     * Equipo 2 - C�digo: SKY, A�o de fundaci�n gal�n: 2009, Pa�s: Reino Unido, Nombre del equipo: Team Sky, Ruta imagen: teamSky.png. <br>
     * Equipo 3 - C�digo: COF, A�o de fundaci�n gal�n: 1997, Pa�s: Francia, Nombre del equipo: Cofidis, Ruta imagen: cofidis.png. <br>
     * Equipo 4 - C�digo: MOT, A�o de fundaci�n gal�n: 2011, Pa�s: Colombia, Nombre del equipo: Movistar Team, Ruta imagen: movistarTeam.png. <br>
     */
    public void inicializar( )
    {
        etapaActual = 1;

        // Inicializa el equipo 1
        equipo1 = new Equipo( );
        equipo1.inicializar( "TNK", 2016, "Rusia", "Tinkoff", "tinkoff.png" );

        // Inicializa el equipo 2
        equipo2 = new Equipo( );
        equipo2.inicializar( "SKY", 2009, "Reino Unido", "Team Sky", "teamSky.png" );

        // TODO Parte 3 Punto C: Completar el m�todo seg�n la documentaci�n dada para el equipo3.

        // TODO Parte 3 Punto D: Completar el m�todo seg�n la documentaci�n dada para el equipo4.
    }

    /**
     * Retorna al equipo 1.
     * @return Equipo 1.
     */
    public Equipo darEquipo1( )
    {
        return equipo1;
    }

    /**
     * Retorna al equipo 2.
     * @return Equipo 2.
     */
    public Equipo darEquipo2( )
    {
        return equipo2;
    }

    /**
     * Retorna al equipo 3.
     * @return Equipo 3.
     */
    public Equipo darEquipo3( )
    {
        // TODO Parte 3 Punto E: Completar el m�todo seg�n la documentaci�n dada
    }

    /**
     * Retorna al equipo 4.
     * @return Equipo 4.
     */
    public Equipo darEquipo4( )
    {
        // TODO Parte 3 Punto F: Completar el m�todo seg�n la documentaci�n dada
    }

    /**
     * Retorna la etapa actual.
     * @return Etapa actual.
     */
    public int darEtapaActual( )
    {
        return etapaActual;
    }

    /**
     * Avanza la etapa actual.<br>
     * <b>post: </b> La etapa actual aument� en 1.
     */
    public void avanzarEtapa( )
    {
        etapaActual = etapaActual + 1;
    }

    /**
     * Registra el puntaje dado por par�metro para el equipo 1. <br>
     * <b>post: </b> Se registr� el puntaje para el equipo 1.<br>
     * @param pPuntaje Puntaje a agregar el equipo 1. pPuntaje >= 0.
     */
    public void registrarPuntajeEquipo1( int pPuntaje )
    {
        equipo1.registrarPuntaje( pPuntaje );
    }

    /**
     * Registra el puntaje dado por par�metro para el equipo 2. <br>
     * <b>post: </b> Se registr� el puntaje para el equipo 2.<br>
     * @param pPuntaje Puntaje a agregar el equipo 2. pPuntaje >= 0.
     */
    public void registrarPuntajeEquipo2( int pPuntaje )
    {
        equipo2.registrarPuntaje( pPuntaje );
    }

    /**
     * Registra el puntaje dado por par�metro para el equipo 3. <br>
     * <b>post: </b> Se registr� el puntaje para el equipo 3.<br>
     * @param pPuntaje Puntaje a agregar el equipo 3. pPuntaje >= 0.
     */
    public void registrarPuntajeEquipo3( int pPuntaje )
    {
        // TODO Parte 3 Punto G: Completar el m�todo seg�n la documentaci�n dada
    }

    /**
     * Registra el puntaje dado por par�metro para el equipo 4. <br>
     * <b>post: </b> Se registr� el puntaje para el equipo 4.<br>
     * @param pPuntaje Puntaje a agregar el equipo 4. pPuntaje >= 0.
     */
    public void registrarPuntajeEquipo4( int pPuntaje )
    {
        equipo4.registrarPuntaje( pPuntaje );
    }

    /**
     * Registra la penalizaci�n dada para el equipo 1.<br>
     * <b>post: </b> Se registr� la penalizaci�n para el equipo 1.<br>
     * @param pPenalizacion Nuevo penalizaci�n del equipo 1. pPenalizacion >= 0.
     */
    public void registrarPenalizacionEquipo1( int pPenalizacion )
    {
        equipo1.registrarPenalizacion( pPenalizacion );
    }

    /**
     * Registra la penalizaci�n dada para el equipo 2.<br>
     * <b>post: </b> Se registr� la penalizaci�n para el equipo 2.<br>
     * @param pPenalizacion Nuevo penalizaci�n del equipo 2. pPenalizacion >= 0.
     */
    public void registrarPenalizacionEquipo2( int pPenalizacion )
    {
        equipo2.registrarPenalizacion( pPenalizacion );
    }

    /**
     * Registra la penalizaci�n dada para el equipo 3.<br>
     * <b>post: </b> Se registr� la penalizaci�n para el equipo 3.<br>
     * @param pPenalizacion Nuevo penalizaci�n del equipo 3. pPenalizacion >= 0.
     */
    public void registrarPenalizacionEquipo3( int pPenalizacion )
    {
        equipo3.registrarPenalizacion( pPenalizacion );
    }

    /**
     * Registra la penalizaci�n dada para el equipo 4.<br>
     * <b>post: </b> Se registr� la penalizaci�n para el equipo 4.<br>
     * @param pPenalizacion Nuevo penalizaci�n del equipo 4. pPenalizacion >= 0.
     */
    public void registrarPenalizacionEquipo4( int pPenalizacion )
    {
        // TODO Parte 3 Punto H: Completar el m�todo seg�n la documentaci�n dada
    }

    /**
     * Retorna el puntaje total de los equipos.
     * @return Suma de los puntajes de cada equipo.
     */
    public int calcularPuntajeTotal( )
    {
        // TODO Parte 3 Punto I: Completar el m�todo seg�n la documentaci�n dada.
    }

    /**
     * Retorna el promedio de los puntajes entre los equipos.
     * @return Promedio total de los puntajes de los equipos (Puntaje total sobre la cantidad de equipos).
     */
    public double calcularPuntajePromedio( )
    {
        // TODO Parte 3 Punto J: Completar el m�todo seg�n la documentaci�n dada.
    }

    /**
     * Calcula el promedio de los puntajes por etapas.
     * @return Puntaje promedio obtenido por etapa (Puntaje promedio sobre la cantidad de etapas transcurridas).
     */
    public double calcularPuntajePromedioPorEtapa( )
    {
        double promedio = calcularPuntajePromedio( ) / etapaActual;
        return promedio;
    }

    /**
     * Retorna el porcentaje de etapas que fueron terminadas con respecto a la cantidad total de etapas.
     * @return Porcentaje de etapas terminadas.
     */
    public double calcularPorcentajeEtapasTerminadas( )
    {
        // TODO Parte 3 Punto K: Completar el m�todo seg�n la documentaci�n dada.
    }

    /**
     * Reinicia la informaci�n deportiva (puntaje) de los equipos y la etapa actual.<br>
     * <b> post: </b> Todos los equipos fueron reiniciados.<br>
     * La etapaActual se modific� a 1.
     */
    public void reiniciar( )
    {
        equipo1.reiniciar( );
        equipo2.reiniciar( );
        // TODO Parte 3 Punto L: Completar el m�todo seg�n la documentaci�n dada.
    }

    // -----------------------------------------------------------------
    // Puntos de Extensi�n
    // -----------------------------------------------------------------

    /**
     * M�todo para la extensi�n 1.
     * @return respuesta1.
     */
    public String metodo1( )
    {
        return "Respuesta 1.";
    }

    /**
     * M�todo para la extensi�n 2.
     * @return respuesta2.
     */
    public String metodo2( )
    {
        return "Respuesta 2.";
    }

}
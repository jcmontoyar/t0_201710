/**
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id: InterfazCupiTourColombia.java,v 1.8   Exp $
 * Universidad de los Andes (Bogot� - Colombia)
 * Departamento de Ingenier�a de Sistemas y Computaci�n 
 * Licenciado bajo el esquema Academic Free License version 2.1 
 *
 * Proyecto Cupi2 (http://cupi2.uniandes.edu.co)
 * Ejercicio: n1_cupiTourColombia
 * Autor: Equipo Cupi2
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package uniandes.cupi2.cupiTourColombia.interfaz;

import uniandes.cupi2.cupiTourColombia.mundo.Equipo;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

/**
 * Panel con la informaci�n de un equipo.
 */
public class PanelEquipo extends JPanel implements ActionListener
{

    // -----------------------------------------------------------------
    // Constantes
    // -----------------------------------------------------------------

    /**
     * Comando para ejecutar la acci�n del bot�n registrar puntaje.
     */
    public final static String REGISTRAR_PUNTAJE = "REGISTRAR_PUNTAJE";

    /**
     * Comando para ejecutar la acci�n del bot�n registrar penalizaci�n.
     */
    public final static String REGISTRAR_PENALIZACION = "REGISTRAR_PENALIZACION";

    // -----------------------------------------------------------------
    // Atributos
    // -----------------------------------------------------------------

    /**
     * Ventana principal de la aplicaci�n.
     */
    private InterfazCupiTourColombia principal;

    // -----------------------------------------------------------------
    // Atributos de la interfaz
    // -----------------------------------------------------------------
    /**
     * N�mero del equipo.
     */
    private Equipo equipo;

    /**
     * Bot�n registrar puntaje del equipo.
     */
    private JButton btnRegistrarPuntaje;

    /**
     * Bot�n registrar penalizaci�n del equipo.
     */
    private JButton btnRegistrarPenalizacion;

    /**
     * Campo de texto para mostrar el c�digo UCI del equipo.
     */
    private JTextField txtCodigo;


    /**
     * Campo de texto para mostrar el a�o de fundaci�n del equipo.
     */
    private JTextField txtAnhoFundacion;

    /**
     * Campo de texto para mostrar el pa�s del equipo.
     */
    private JTextField txtPais;

    /**
     * Campo de texto para mostrar el puntaje del equipo.
     */
    private JTextField txtPuntaje;

    // -----------------------------------------------------------------
    // Constructores
    // -----------------------------------------------------------------

    /**
     * Constructor del panel.<br>
     * <b>post: </b> Construy� el panel e inicializ� la ventana con el atributo.<br>
     * @param pPrincipal Referencia a la ventana principal. pPrincipal != null.
     * @param pEquipo Equipo a mostrar. pEquipo != null && pEquipo != ""}.
     */
    public PanelEquipo( InterfazCupiTourColombia pPrincipal, Equipo pEquipo )
    {
        principal = pPrincipal;
        equipo = pEquipo;
        setBorder( new TitledBorder( pEquipo.darNombre( ) ) );
        setLayout( new BorderLayout( ) );
        setPreferredSize( new Dimension( 100, 440 ) );

        // Etiqueta de Imagen
        ImageIcon icono = new ImageIcon( "data/imagenes/" + equipo.darRutaImagen( ) );
        JLabel lblImagen = new JLabel( );
        lblImagen.setIcon( icono );
        lblImagen.setBorder( new EmptyBorder( 4, 4, 4, 4 ) );
        add( lblImagen, BorderLayout.WEST );

        // Panel con la informaci�n
        JPanel panelInfo = new JPanel( );
        panelInfo.setLayout( new GridLayout( 4, 2, 4, 4 ) );

        JLabel lblCodigo = new JLabel( "C�digo UCI: " );
        panelInfo.add( lblCodigo );

        txtCodigo = new JTextField( equipo.darCodigo( ) );
        panelInfo.add( txtCodigo );

        JLabel lblAnhoFundacion = new JLabel( "A�o fundaci�n: " );
        panelInfo.add( lblAnhoFundacion );

        txtAnhoFundacion = new JTextField( "" + equipo.darAnioFundacion( ) );
        panelInfo.add( txtAnhoFundacion );

        JLabel lblPais = new JLabel( "Pa�s: " );
        panelInfo.add( lblPais );

        txtPais = new JTextField( equipo.darPais( ) );
        panelInfo.add( txtPais );

        JLabel lblPuntaje = new JLabel( "Puntaje: " );
        panelInfo.add( lblPuntaje );

        txtPuntaje = new JTextField( equipo.darPuntaje( ) );
        panelInfo.add( txtPuntaje );

        add( panelInfo, BorderLayout.CENTER );

        JPanel panelBotones = new JPanel( );
        panelBotones.setLayout( new GridLayout( 1, 2, 5, 5 ) );

        // Bot�n Registrar puntaje

        btnRegistrarPuntaje = new JButton( "Registrar puntaje" );
        btnRegistrarPuntaje.setActionCommand( REGISTRAR_PUNTAJE );
        btnRegistrarPuntaje.addActionListener( this );
        panelBotones.add( btnRegistrarPuntaje );

        // Bot�n Registrar penalizaci�n

        btnRegistrarPenalizacion = new JButton( "Registrar penalizaci�n" );
        btnRegistrarPenalizacion.setActionCommand( REGISTRAR_PENALIZACION );
        btnRegistrarPenalizacion.addActionListener( this );
        panelBotones.add( btnRegistrarPenalizacion );

        add( panelBotones, BorderLayout.SOUTH );

    }

    // -----------------------------------------------------------------
    // M�todos
    // -----------------------------------------------------------------

    /**
     * Activa los botones de registrar penalizaci�n y registrar puntaje.
     */
    public void activarBotones( )
    {
        btnRegistrarPenalizacion.setEnabled( true );
        btnRegistrarPuntaje.setEnabled( true );
    }

    /**
     * Desactiva los botones de registrar penalizaci�n y registrar puntaje.
     */
    public void desactivarBotones( )
    {
        btnRegistrarPenalizacion.setEnabled( false );
        btnRegistrarPuntaje.setEnabled( false );
    }

    /**
     * Actualiza la informaci�n del equipo actual con la informaci�n que llega por par�metro.
     * @param pPuntaje Nuevo puntaje del equipo. pPuntaje >= 0 && pPuntaje != null.
     */
    public void actualizar( int pPuntaje )
    {
        txtPuntaje.setText( ""+pPuntaje );
    }

    /**
     * Manejo de eventos del usuario.
     * @param pEvento Evento de usuario. pEvento != null.
     */
    public void actionPerformed( ActionEvent pEvento )
    {
        String comando = pEvento.getActionCommand( );
        if( comando.equals( REGISTRAR_PUNTAJE ) )
        {
            principal.registrarPuntaje( equipo.darCodigo( ) );
        }
        else if( comando.equals( REGISTRAR_PENALIZACION ) )
        {
            principal.registrarPenalizacion( equipo.darCodigo( ) );
        }

    }

}
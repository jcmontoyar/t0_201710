/**
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id: InterfazCupiTourColombia.java,v 1.8   Exp $
 * Universidad de los Andes (Bogot� - Colombia)
 * Departamento de Ingenier�a de Sistemas y Computaci�n 
 * Licenciado bajo el esquema Academic Free License version 2.1 
 *
 * Proyecto Cupi2 (http://cupi2.uniandes.edu.co)
 * Ejercicio: n1_cupiTourColombia
 * Autor: Equipo Cupi2
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
package uniandes.cupi2.cupiTourColombia.interfaz;

import javax.swing.border.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Panel de manejo de acciones.
 */
public class PanelOpciones extends JPanel implements ActionListener
{

    // -----------------------------------------------------------------
    // Constantes
    // -----------------------------------------------------------------

    /**
     * Comando para ejecutar la acci�n del bot�n reiniciar.
     */
    public final static String REINICIAR = "REINICIAR";

    /**
     * Comando para ejecutar la acci�n del bot�n opci�n 1.
     */
    public final static String OPCION_1 = "OPCION_1";

    /**
     * Comando para ejecutar la acci�n del bot�n opci�n 2.
     */
    public final static String OPCION_2 = "OPCION_2";

    // -----------------------------------------------------------------
    // Atributos
    // -----------------------------------------------------------------

    /**
     * Bot�n para reiniciar.
     */
    private JButton btnReiniciar;

    /**
     * Bot�n para la opci�n 1.
     */
    private JButton btnOpcion1;
    /**
     * Bot�n para la opci�n 2.
     */
    private JButton btnOpcion2;

    /**
     * Ventana principal de la aplicaci�n.
     */
    private InterfazCupiTourColombia principal;

    // -----------------------------------------------------------------
    // Constructores
    // -----------------------------------------------------------------

    /**
     * Construye el panel de acciones con una referencia a la ventana principal de la aplicaci�n. <br>
     * <b>post: </b> Construy� el panel e inicializ� la ventana con el atributo.
     * @param pPrincipal Referencia a la ventana principal. pPrincipal != null.
     */
    public PanelOpciones( InterfazCupiTourColombia pPrincipal )
    {
        principal = pPrincipal;

        setBorder( new TitledBorder( "Opciones" ) );
        setLayout( new GridLayout( 1, 3 ) );

        btnReiniciar = new JButton( "Reiniciar " );
        btnReiniciar.setActionCommand( REINICIAR );
        btnReiniciar.addActionListener( this );
        add( btnReiniciar );

        btnOpcion1 = new JButton( "Opci�n 1 " );
        btnOpcion1.setActionCommand( OPCION_1 );
        btnOpcion1.addActionListener( this );
        add( btnOpcion1 );

        btnOpcion2 = new JButton( "Opci�n 2 " );
        btnOpcion2.setActionCommand( OPCION_2 );
        btnOpcion2.addActionListener( this );
        add( btnOpcion2 );

    }

    // -----------------------------------------------------------------
    // M�todos
    // -----------------------------------------------------------------

    /**
     * Manejo de eventos del usuario.
     * @param pEvento Evento de usuario. pEvento != null.
     */
    public void actionPerformed( ActionEvent pEvento )
    {
        String comando = pEvento.getActionCommand( );
        if( comando.equals( REINICIAR ) )
        {
            principal.reiniciar( );
        }
        else if( comando.equals( OPCION_1 ) )
        {
            principal.reqFuncOpcion1( );
        }
        else if( comando.equals( OPCION_2 ) )
        {
            principal.reqFuncOpcion2( );
        }

    }

}
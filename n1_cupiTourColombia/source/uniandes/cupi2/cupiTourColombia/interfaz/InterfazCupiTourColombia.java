/**
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id: InterfazCupiTourColombia.java,v 1.6   Exp $
 * Universidad de los Andes (Bogot� - Colombia)
 * Departamento de Ingenier��a de Sistemas y Computaci�n 
 * Licenciado bajo el esquema Academic Free License version 2.1 
 *
 * Proyecto Cupi2 (http://cupi2.uniandes.edu.co)
 * Ejercicio: n1_cupiTourColombia
 * Autor: Equipo Cupi2
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package uniandes.cupi2.cupiTourColombia.interfaz;

import javax.swing.*;

import uniandes.cupi2.cupiTourColombia.mundo.Equipo;
import uniandes.cupi2.cupiTourColombia.mundo.CupiTourColombia;

import java.awt.*;

/**
 * Ventana principal de la aplicaci�n.
 */
public class InterfazCupiTourColombia extends JFrame
{

    // -----------------------------------------------------------------
    // Constantes
    // -----------------------------------------------------------------

    /**
     * Constante que representa el equipo 1.
     */
    public final static int EQUIPO_1 = 1;

    /**
     * Constante que representa el equipo 2.
     */
    public final static int EQUIPO_2 = 2;

    /**
     * Constante que representa el equipo 3.
     */
    public final static int EQUIPO_3 = 3;

    /**
     * Constante que representa el equipo 4.
     */
    public final static int EQUIPO_4 = 4;

    // -----------------------------------------------------------------
    // Atributos
    // -----------------------------------------------------------------

    /**
     * Clase principal del mundo.
     */
    private CupiTourColombia cupiTourColombia;

    /**
     * Atributo que representa si el equipo 1 ya registr� alguna penalizaci�n o puntaje en la etapa que se encuentra.
     */
    private boolean registroEquipo1;

    /**
     * Atributo que representa si el equipo 2 ya registr� alguna penalizaci�n o puntaje en la etapa que se encuentra.
     */
    private boolean registroEquipo2;

    /**
     * Atributo que representa si el equipo 3 ya registr� alguna penalizaci�n o puntaje en la etapa que se encuentra.
     */
    private boolean registroEquipo3;

    /**
     * Atributo que representa si el equipo 4 ya registr� alguna penalizaci�n o puntaje en la etapa que se encuentra.
     */
    private boolean registroEquipo4;

    // -----------------------------------------------------------------
    // Atributos de la interfaz
    // -----------------------------------------------------------------

    /**
     * Panel con la imagen del encabezado.
     */
    private PanelImagen panelImagen;

    /**
     * Panel con la informaci�n general del tour.
     */
    private PanelInformacion panelInformacion;

    /**
     * Panel con la informaci�n del Equipo 1.
     */
    private PanelEquipo panelEquipo1;

    /**
     * Panel con la informaci�n del Equipo 2.
     */
    private PanelEquipo panelEquipo2;

    /**
     * Panel con la informaci�n del Equipo 3.
     */
    private PanelEquipo panelEquipo3;

    /**
     * Panel con la informaci�n del Equipo 4.
     */
    private PanelEquipo panelEquipo4;

    /**
     * Panel con las opciones.
     */
    private PanelOpciones panelOpciones;

    // -----------------------------------------------------------------
    // Constructores
    // -----------------------------------------------------------------

    /**
     * Constructor de la ventana principal.
     */
    public InterfazCupiTourColombia( )
    {
        // Crea la clase principal.
        cupiTourColombia = new CupiTourColombia( );
        cupiTourColombia.inicializar( );

        // Construye la forma.
        setTitle( "CupiTourColombia" );
        setLayout( new BorderLayout( ) );
        setSize( 810, 720 );
        setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
        setLocationRelativeTo( null );

        panelImagen = new PanelImagen( );
        add( panelImagen, BorderLayout.NORTH );

        panelOpciones = new PanelOpciones( this );
        add( panelOpciones, BorderLayout.SOUTH );

        JPanel centro = new JPanel( );
        centro.setLayout( new BorderLayout( ) );

        JPanel equipos = new JPanel( );
        equipos.setLayout( new GridLayout( 2, 2 ) );

        panelInformacion = new PanelInformacion( this );
        centro.add( panelInformacion, BorderLayout.NORTH );

        panelEquipo1 = new PanelEquipo( this, cupiTourColombia.darEquipo1( ) );
        equipos.add( panelEquipo1 );

        panelEquipo2 = new PanelEquipo( this, cupiTourColombia.darEquipo2( ) );
        equipos.add( panelEquipo2 );

        panelEquipo3 = new PanelEquipo( this, cupiTourColombia.darEquipo3( ) );
        equipos.add( panelEquipo3 );

        panelEquipo4 = new PanelEquipo( this, cupiTourColombia.darEquipo4( ) );
        equipos.add( panelEquipo4 );

        centro.add( equipos, BorderLayout.CENTER );

        add( centro, BorderLayout.CENTER );

        registroEquipo1 = false;
        registroEquipo2 = false;
        registroEquipo3 = false;
        registroEquipo4 = false;

        // Centrar la ventana.
        setLocationRelativeTo( null );
        
        // Actualizar la informaci�n de los paneles
        actualizarPaneles( );

    }

    // -----------------------------------------------------------------
    // M�todos
    // -----------------------------------------------------------------

    /**
     * Devuelve el equipo con el n�mero dado por par�metro.<br>
     * <b> pre: </b> El atributo del mundo ha sido inicializado.
     * @param pEquipo N�mero del equipo que se le registra el puntaje. pNumeroEquipo != null && pNumeroEquipo pertenece a {EQUIPO_1,EQUIPO_2,EQUIPO_3,EQUIPO_4}.
     * @return Equipo Equipo solicitado por par�metro.
     */
    public Equipo darEquipo( int pEquipo )
    {
        Equipo equipo = null;

        if( pEquipo == EQUIPO_1 )
        {
            equipo = cupiTourColombia.darEquipo1( );
        }
        else if( pEquipo == EQUIPO_2 )
        {
            equipo = cupiTourColombia.darEquipo2( );
        }
        else if( pEquipo == EQUIPO_3 )
        {
            equipo = cupiTourColombia.darEquipo3( );
        }
        else if( pEquipo == EQUIPO_4 )
        {
            equipo = cupiTourColombia.darEquipo4( );
        }
        return equipo;
    }

    /**
     * Avanza la etapa del tour <b> post: </b> La etapa se avanz� en uno. <br>
     * La informaci�n en la interfaz fue actualizada.
     */
    public void avanzarEtapa( )
    {
        cupiTourColombia.avanzarEtapa( );
        if( registroEquipo1 && registroEquipo2 && registroEquipo3 && registroEquipo4 )
        {
            panelEquipo1.activarBotones( );
            panelEquipo2.activarBotones( );
            panelEquipo3.activarBotones( );
            panelEquipo4.activarBotones( );
            registroEquipo1 = false;
            registroEquipo2 = false;
            registroEquipo3 = false;
            registroEquipo4 = false;
            panelInformacion.cambiarEstadoBoton( );
        }
        if( cupiTourColombia.darEtapaActual( ) == 6 )
        {
            panelEquipo1.desactivarBotones( );
            panelEquipo2.desactivarBotones( );
            panelEquipo3.desactivarBotones( );
            panelEquipo4.desactivarBotones( );
        }

        panelInformacion.actualizar( cupiTourColombia.darEtapaActual( ), cupiTourColombia.calcularPuntajePromedio( ), cupiTourColombia.calcularPuntajeTotal( ), cupiTourColombia.calcularPuntajePromedioPorEtapa( ),
                cupiTourColombia.calcularPorcentajeEtapasTerminadas( ) );

    }

    /**
     * Registra el puntaje al equipo dado por par�metro.<br>
     * <b> pre: </b> El atributo del mundo ha sido inicializado.<br>
     * <b> post: </b> Se ha registrado el puntaje y avanzado la etapa en uno al equipo dado por par�metro.
     * @param pCodigoEquipo C�digo del equipo al que se le registra el puntaje. pNumeroEquipo != null && pNumeroEquipo pertenece a {EQUIPO_1,EQUIPO_2,EQUIPO_3,EQUIPO_4}.
     */
    public void registrarPuntaje( String pCodigoEquipo )
    {
        try
        {
            int puntaje = Integer.parseInt( JOptionPane.showInputDialog( this, "Ingrese el puntaje recibido en la etapa.", "Puntaje", JOptionPane.QUESTION_MESSAGE ) );

            if( puntaje >= 0 )
            {
                if( pCodigoEquipo.equals( cupiTourColombia.darEquipo1( ).darCodigo( ) ) )
                {
                    registroEquipo1 = true;
                    cupiTourColombia.registrarPuntajeEquipo1( puntaje );
                    panelEquipo1.desactivarBotones( );
                }
                else if( pCodigoEquipo.equals( cupiTourColombia.darEquipo2( ).darCodigo( ) ) )
                {
                    registroEquipo2 = true;
                    cupiTourColombia.registrarPuntajeEquipo2( puntaje );
                    panelEquipo2.desactivarBotones( );
                }
                else if( pCodigoEquipo.equals( cupiTourColombia.darEquipo3( ).darCodigo( ) ) )
                {
                    registroEquipo3 = true;
                    cupiTourColombia.registrarPuntajeEquipo3( puntaje );
                    panelEquipo3.desactivarBotones( );
                }
                else if( pCodigoEquipo.equals( cupiTourColombia.darEquipo4( ).darCodigo( ) ) )
                {
                    registroEquipo4 = true;
                    cupiTourColombia.registrarPuntajeEquipo4( puntaje );
                    panelEquipo4.desactivarBotones( );
                }
                if( registroEquipo1 && registroEquipo2 && registroEquipo3 && registroEquipo4 )
                {
                    panelInformacion.cambiarEstadoBoton( );
                }
                actualizarPaneles( );

            }
            else
            {
                JOptionPane.showMessageDialog( this, "El puntaje debe ser mayor o igual a cero.", "Registrar puntaje", JOptionPane.ERROR_MESSAGE );
            }
        }
        catch( NumberFormatException e )
        {
            JOptionPane.showMessageDialog( this, "Ingrese valores num�ricos.", "Registrar puntaje", JOptionPane.ERROR_MESSAGE );
        }

    }
    /**
     * Registra la penalizaci�n al equipo dado por par�metro.<br>
     * <b> pre: </b> El atributo del mundo ha sido inicializado.<br>
     * <b> post: </b> Se ha registrado la penalizaci�n y avanzado la etapa en uno al equipo dado por par�metro.
     * @param pCodigoEquipo C�digo del equipo al que se le registra el puntaje. pNumeroEquipo != null && pNumeroEquipo pertenece a {EQUIPO_1,EQUIPO_2,EQUIPO_3,EQUIPO_4}.
     */
    public void registrarPenalizacion( String pCodigoEquipo )
    {
        try
        {
            int penalizacion = Integer.parseInt( JOptionPane.showInputDialog( this, "Ingrese la penalizaci�n recibida en la etapa.", "Penalizaci�n", JOptionPane.QUESTION_MESSAGE ) );

            if( penalizacion >= 0 )
            {
                if( pCodigoEquipo.equals( cupiTourColombia.darEquipo1( ).darCodigo( ) ) )
                {
                    registroEquipo1 = true;
                    cupiTourColombia.registrarPenalizacionEquipo1( penalizacion );
                    panelEquipo1.desactivarBotones( );
                }
                else if( pCodigoEquipo.equals( cupiTourColombia.darEquipo2( ).darCodigo( ) ) )
                {
                    registroEquipo2 = true;
                    cupiTourColombia.registrarPenalizacionEquipo2( penalizacion );
                    panelEquipo2.desactivarBotones( );
                }
                else if( pCodigoEquipo.equals( cupiTourColombia.darEquipo3( ).darCodigo( ) ) )
                {
                    registroEquipo3 = true;
                    cupiTourColombia.registrarPenalizacionEquipo3( penalizacion );
                    panelEquipo3.desactivarBotones( );
                }
                else if( pCodigoEquipo.equals( cupiTourColombia.darEquipo4( ).darCodigo( ) ) )
                {
                    registroEquipo4 = true;
                    cupiTourColombia.registrarPenalizacionEquipo4( penalizacion );
                    panelEquipo4.desactivarBotones( );
                }
                if( registroEquipo1 && registroEquipo2 && registroEquipo3 && registroEquipo4 )
                {

                    panelInformacion.cambiarEstadoBoton( );
                }
                actualizarPaneles( );

            }
            else
            {
                JOptionPane.showMessageDialog( this, "La penalizaci�n debe ser mayor o igual a cero.", "Registrar penalizaci�n", JOptionPane.ERROR_MESSAGE );
            }
        }
        catch( NumberFormatException e )
        {
            JOptionPane.showMessageDialog( this, "Ingrese valores num�ricos.", "Registrar penalizaci�n", JOptionPane.ERROR_MESSAGE );
        }

    }

    /**
     * Reinicia la informaci�n del tour y los equipos.
     */
    public void reiniciar( )
    {
        registroEquipo1 = false;
        registroEquipo2 = false;
        registroEquipo3 = false;
        registroEquipo4 = false;
        panelEquipo1.activarBotones( );
        panelEquipo2.activarBotones( );
        panelEquipo3.activarBotones( );
        panelEquipo4.activarBotones( );
        cupiTourColombia.reiniciar( );
        actualizarPaneles( );
    }

    /**
     * Actualiza los campos de la interfaz con la informaci�n del tour y sus equipos.
     */
    private void actualizarPaneles( )
    {
        panelInformacion.actualizar( cupiTourColombia.darEtapaActual( ), cupiTourColombia.calcularPuntajePromedio( ), cupiTourColombia.calcularPuntajeTotal( ), cupiTourColombia.calcularPuntajePromedioPorEtapa( ),
                cupiTourColombia.calcularPorcentajeEtapasTerminadas( ) );
        panelEquipo1.actualizar( cupiTourColombia.darEquipo1( ).darPuntaje( ) );
        panelEquipo2.actualizar( cupiTourColombia.darEquipo2( ).darPuntaje( ) );
        panelEquipo3.actualizar( cupiTourColombia.darEquipo3( ).darPuntaje( ) );
        panelEquipo4.actualizar( cupiTourColombia.darEquipo4( ).darPuntaje( ) );
    }
    /**
     * M�todo para la extensi�n 1.
     */
    public void reqFuncOpcion1( )
    {
        String resultado = cupiTourColombia.metodo1( );
        JOptionPane.showMessageDialog( this, resultado, "Respuesta", JOptionPane.INFORMATION_MESSAGE );
    }

    /**
     * M�todo para la extensi�n 2.
     */
    public void reqFuncOpcion2( )
    {
        String resultado = cupiTourColombia.metodo2( );
        JOptionPane.showMessageDialog( this, resultado, "Respuesta", JOptionPane.INFORMATION_MESSAGE );
    }

    /**
     * Ejecuta la aplicaci�n, creando una nueva interfaz.
     * @param pArgs Argumentos del sistema para iniciar la aplicaci�n. pArgs != null.
     */
    public static void main( String[] pArgs )
    {
        // Unifica la interfaz para Mac y para Windows.
        try
        {
            UIManager.setLookAndFeel( UIManager.getCrossPlatformLookAndFeelClassName( ) );
        }
        catch( Exception e )
        {
            e.printStackTrace( );
        }
        InterfazCupiTourColombia interfaz = new InterfazCupiTourColombia( );
        interfaz.setVisible( true );
    }

}
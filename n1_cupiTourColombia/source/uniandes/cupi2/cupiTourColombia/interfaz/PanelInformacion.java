/**
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id: InterfazCupiTourColombia.java,v 1.8   Exp $
 * Universidad de los Andes (Bogot� - Colombia)
 * Departamento de Ingenier�a de Sistemas y Computaci�n 
 * Licenciado bajo el esquema Academic Free License version 2.1 
 *
 * Proyecto Cupi2 (http://cupi2.uniandes.edu.co)
 * Ejercicio: n1_cupiTourColombia
 * Autor: Equipo Cupi2
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
package uniandes.cupi2.cupiTourColombia.interfaz;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.border.*;
import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Panel de informaci�n adicional.
 */
public class PanelInformacion extends JPanel implements ActionListener
{

    // -----------------------------------------------------------------
    // Constantes
    // -----------------------------------------------------------------
    /**
     * Comando para avanzar una etapa.
     */
    private final static String AVANZAR = "Avanzar";

    // -----------------------------------------------------------------
    // Atributos
    // -----------------------------------------------------------------
    /**
     * Ventana principal de la aplicaci�n.
     */
    private InterfazCupiTourColombia principal;

    // -----------------------------------------------------------------
    // Atributos de la interfaz
    // -----------------------------------------------------------------
    /**
     * Bot�n para avanzar la etapa actual
     */
    private JButton btnAvanzar;

    /**
     * Etiqueta para mostrar la etapa actual del tour.
     */
    private JTextField txtEtapa;

    /**
     * Etiqueta para mostrar el promedio de los puntajes de los equipos.
     */
    private JTextField txtPromedio;

    /**
     * Etiqueta para mostrar el promedio del a�o de fundaci�n de los equipos.
     */
    private JTextField txtPromedioEtapas;

    /**
     * Etiqueta para mostrar el total de los puntajes de los equipos.
     */
    private JTextField txtTotalPuntajes;

    /**
     * Campo de texto para mostrar el porcentaje de etapas terminadas.
     */
    private JTextField txtPorcentajeEtapas;

    // -----------------------------------------------------------------
    // Constructores
    // -----------------------------------------------------------------

    /**
     * Construye el panel de informaci�n con una referencia a la ventana principal de la aplicaci�n.<br>
     * <b>post: </b> Se inicializ� el panel de informaci�n y el atributo ventana se inicializ� con el par�metro.
     * @param pPrincipal Ventana principal de la aplicaci�n. pPrincipal != null
     */
    public PanelInformacion( InterfazCupiTourColombia pPrincipal )
    {
        principal = pPrincipal;
        setBorder( new TitledBorder( "Informaci�n General" ) );
        setLayout( new GridLayout( 3, 2, 5, 5 ) );

        add( new JLabel( "Etapa actual:" ) );

        JPanel panelEtapa = new JPanel( );
        panelEtapa.setLayout( new BorderLayout( ) );
        add( panelEtapa );

        txtEtapa = new JTextField( "" );
        txtEtapa.setEditable( false );
        panelEtapa.add( txtEtapa, BorderLayout.CENTER );

        btnAvanzar = new JButton( "Avanzar" );
        btnAvanzar.addActionListener( this );
        btnAvanzar.setActionCommand( AVANZAR );
        btnAvanzar.setEnabled( false );
        panelEtapa.add( btnAvanzar, BorderLayout.EAST );

        add( new JLabel( ) );
        add( new JLabel( ) );

        add( new JLabel( "Porcentaje de etapas terminadas:" ) );
        txtPorcentajeEtapas = new JTextField( );
        txtPorcentajeEtapas.setEditable( false );
        add( txtPorcentajeEtapas );

        add( new JLabel( " Puntaje promedio por etapa:" ) );
        txtPromedioEtapas = new JTextField( );
        txtPromedioEtapas.setEditable( false );
        add( txtPromedioEtapas, BorderLayout.CENTER );

        add( new JLabel( "Puntaje promedio:" ) );
        txtPromedio = new JTextField( " " );
        txtPromedio.setEditable( false );
        add( txtPromedio );

        add( new JLabel( "Puntaje total:" ) );
        txtTotalPuntajes = new JTextField( "" );
        txtTotalPuntajes.setEditable( false );
        add( txtTotalPuntajes );

    }
    // -----------------------------------------------------------------
    // M�todos
    // -----------------------------------------------------------------

    /**
     * Actualiza los campos de la interfaz con la informaci�n del tour.<br>
     * @param pEtapaActual Etapa actual del tour.
     * @param pPromedio Promedio del total de los puntajes entre los equipos. pPromedio >=0.
     * @param pPuntajeTotal Puntaje total de todos los equipos. pPuntajeTotal >=0.
     * @param pPuntajePromedioEtapa Puntaje promedio por etapas. pPromedioFundaciones >= 0.
     * @param pPorcentajeEtapasTerminadas Porcentaje de etapas terminadas. pPorcentajeEtapasTerminadas >= 0
     */
    public void actualizar( double pEtapaActual, double pPromedio, int pPuntajeTotal, double pPuntajePromedioEtapa, double pPorcentajeEtapasTerminadas )
    {
        if( pEtapaActual == 6 )
        {
            txtEtapa.setText( "Finalizado" );
        }
        else
        {
            txtEtapa.setText( pEtapaActual + "" );
        }
        txtPromedio.setText( pPromedio + "" );
        txtTotalPuntajes.setText( pPuntajeTotal + "" );
        txtPromedioEtapas.setText( pPuntajePromedioEtapa + "" );
        txtPorcentajeEtapas.setText( pPorcentajeEtapasTerminadas + "%" );
    }

    /**
     * Deshabilita el bot�n de avance de etapa.<br>
     * <b> post: </b> El bot�n de avance de etapa est� deshabilitado.
     */
    public void cambiarEstadoBoton( )
    {
        btnAvanzar.setEnabled( !btnAvanzar.isEnabled( ) );

    }
    /**
     * M�todo que maneja las acciones de los botones.
     */
    public void actionPerformed( ActionEvent e )
    {
        if( e.getActionCommand( ).equals( AVANZAR ) )
        {
            principal.avanzarEtapa( );
        }
       
    }
}
#!/bin/sh
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Universidad de los Andes (Bogotá - Colombia)
# Departamento de Ingeniería de Sistemas y Computación
# Licenciado bajo el esquema Academic Free License version 2.1
#
# Proyecto Cupi2 (http://cupi2.uniandes.edu.co)
# Ejercicio: n1_cupiTourColombia
# Autor: Equipo Cupi2 2017
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

stty -echo

# ---------------------------------------------------------
# Ejecución de las pruebas
# ---------------------------------------------------------

cd ../..
	
java -ea -classpath ./lib/cupiTourColombia.jar:./test/lib/cupiTourColombiaTest.jar:./test/lib/junit.jar junit.swingui.TestRunner uniandes.cupi2.cupiTourColombia.test.EquipoTest
java -ea -classpath ./lib/cupiTourColombia.jar:./test/lib/cupiTourColombiaTest.jar:./test/lib/junit.jar junit.swingui.TestRunner uniandes.cupi2.cupiTourColombia.test.CupiTourColombiaTest

cd bin/mac

stty echo

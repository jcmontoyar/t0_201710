/**
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id: InterfazCupiTourColombia.java,v 1.8   Exp $
 * Universidad de los Andes (Bogot� - Colombia)
 * Departamento de Ingenier�a de Sistemas y Computaci�n 
 * Licenciado bajo el esquema Academic Free License version 2.1 
 *
 * Proyecto Cupi2 (http://cupi2.uniandes.edu.co)
 * Ejercicio: n1_cupiTourColombia
 * Autor: Equipo Cupi2
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
package uniandes.cupi2.cupiTourColombia.test;

import junit.framework.TestCase;
import uniandes.cupi2.cupiTourColombia.mundo.Equipo;

/**
 * Clase usada para verificar que los m�todos de la clase Equipo est�n correctamente implementados.
 */
public class EquipoTest extends TestCase
{

    // -------------------------------------------------------------
    // Atributos
    // -------------------------------------------------------------

    /**
     * Equipo donde se har�n las pruebas.
     */
    private Equipo equipo;

    // -------------------------------------------------------------
    // M�todos
    // -------------------------------------------------------------

    /**
     * Escenario 1: Crea un nuevo espacio.
     */
    public void setupEscenario1( )
    {
        equipo = new Equipo( );
        equipo.inicializar( "MOV", 1300, "COLOMBIA", "Movistar", "movistar.png" );
    }

    /**
     * Prueba 1: Se encarga de verificar el m�todo constructor de la clase.<br>
     * <b> M�todos a probar: </b> <br>
     * Equipo<br>
     * darCodigo<br>
     * darNombre<br>
     * darFundacion<br>
     * darPais<br>
     * darPuntaje<br>
     * <b> Caso de prueba:</b><br>
     * 1. Construye correctamente el equipo, cada uno de los valores corresponde al esperado.
     */
    public void testInicializar( )
    {
        setupEscenario1( );

        assertEquals( "No inicializ� el equipo correctamente, el c�digo no corresponde.", "MOV", equipo.darCodigo( ) );
        assertEquals( "No inicializ� el equipo correctamente, el equipo no corresponde.", "Movistar", equipo.darNombre( ) );
        assertEquals( "No inicializ� el equipo correctamente, el a�o de fundaci�n no corresponde.", 1300, equipo.darAnioFundacion( ) );
        assertEquals( "No inicializ� el equipo correctamente, el pa�s no corresponde.", "COLOMBIA", equipo.darPais( ) );
        assertEquals( "No inicializ� el equipo correctamente, el puntaje no corresponde.", 0, equipo.darPuntaje( ) );
    }

    /**
     * Prueba 2: Se encarga de verificar el m�todo registrarPuntaje de la clase.<br>
     * <b> M�todos a probar: </b> <br>
     * registrarPuntaje<br>
     * darPuntaje<br>
     * <b> Caso de prueba:</b><br>
     * 1. El equipo no tiene puntos. <br>
     * 2. El equipo ya tiene puntos.
     */
    public void testRegistrarPuntaje( )
    {
        setupEscenario1( );

        // Caso de prueba 1
        equipo.registrarPuntaje( 150 );
        assertEquals( "No registr� el puntaje correctamente, el puntaje total no corresponde.", 150, equipo.darPuntaje( ) );

        // Caso de prueba 2
        equipo.registrarPuntaje( 50 );
        assertEquals( "No registr� el puntaje correctamente, el puntaje total no corresponde.", 200, equipo.darPuntaje( ) );

    }

    /**
     * Prueba 3: Se encarga de verificar el m�todo registrarPenalizacion.<br>
     * <b> M�todos a probar: </b> <br>
     * registrarPenalizacion<br>
     * registrarPuntaje<br>
     * darPuntaje<br>
     * <b> Caso de prueba:</b><br>
     * 1. El equipo tiene puntos.
     */
    public void testRegistrarPenalizacion( )
    {
        setupEscenario1( );

        equipo.registrarPuntaje( 150 );
        equipo.registrarPenalizacion( 20 );
        assertEquals( "No se registr� la penalizaci�n correctamente, el puntaje no corresponde.", 130, equipo.darPuntaje( ) );

        equipo.registrarPenalizacion( 50 );
        assertEquals( "No se registr� la penalizaci�n correctamente, el puntaje no corresponde.", 80, equipo.darPuntaje( ) );
    }

    /**
     * Prueba 4: Se encarga de verificar el m�todo reiniciar.<br>
     * <b> M�todos a probar: </b> <br>
     * reiniciar<br>
     * darEtapa<br>
     * darPuntaje<br>
     * <b> Caso de prueba:</b><br>
     * 1. El equipo tiene puntos. <br>
     * 2. El equipo no tiene puntos
     */
    public void testReiniciar( )
    {
        setupEscenario1( );

        // Caso de prueba 1
        equipo.reiniciar( );
        assertEquals( "No reinici� correctamente, el puntaje del equipo no corresponde.", 0, equipo.darPuntaje( ) );

        // Caso de prueba 2
        equipo.reiniciar( );
        assertEquals( "No reinici� correctamente, el puntaje del equipo no corresponde.", 0, equipo.darPuntaje( ) );

    }

}
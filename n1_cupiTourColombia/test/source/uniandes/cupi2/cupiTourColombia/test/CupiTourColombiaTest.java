/**
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id: InterfazCupiTourColombia.java,v 1.8   Exp $
 * Universidad de los Andes (Bogot� - Colombia)
 * Departamento de Ingenier�a de Sistemas y Computaci�n 
 * Licenciado bajo el esquema Academic Free License version 2.1 
 *
 * Proyecto Cupi2 (http://cupi2.uniandes.edu.co)
 * Ejercicio: n1_cupiTourColombia
 * Autor: Equipo Cupi2
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
package uniandes.cupi2.cupiTourColombia.test;

import static org.junit.Assert.*;

import org.junit.Test;

import uniandes.cupi2.cupiTourColombia.mundo.CupiTourColombia;
import uniandes.cupi2.cupiTourColombia.mundo.Equipo;

/**
 * Clase usada para verificar que los m�todos de la clase CupiTourColombia est�n correctamente implementados.
 */
public class CupiTourColombiaTest
{

    // -----------------------------------------------------------------
    // Atributos
    // -----------------------------------------------------------------

    /**
     * CupiTourColombia donde se har�n las pruebas.
     */
    private CupiTourColombia cupiTourColombia;

    // -----------------------------------------------------------------
    // M�todos
    // -----------------------------------------------------------------

    /**
     * Construye un nuevo tour de Colombia con 4 equipos sin puntajes.
     */
    private void setupEscenario1( )
    {
        cupiTourColombia = new CupiTourColombia( );
        cupiTourColombia.inicializar( );
    }

    /**
     * Construye un nuevo tour de Colombia con 4 equipos con puntajes.
     */
    private void setupEscenario2( )
    {
        cupiTourColombia = new CupiTourColombia( );
        cupiTourColombia.inicializar( );

        cupiTourColombia.registrarPuntajeEquipo1( 50 );
        cupiTourColombia.registrarPuntajeEquipo2( 100 );
        cupiTourColombia.registrarPuntajeEquipo3( 20 );
        cupiTourColombia.registrarPuntajeEquipo4( 70 );
        cupiTourColombia.avanzarEtapa( );

        cupiTourColombia.registrarPuntajeEquipo1( 10 );
        cupiTourColombia.registrarPuntajeEquipo2( 20 );
        cupiTourColombia.registrarPuntajeEquipo3( 30 );
        cupiTourColombia.registrarPuntajeEquipo4( 40 );
        cupiTourColombia.avanzarEtapa( );

        cupiTourColombia.registrarPuntajeEquipo1( 100 );
        cupiTourColombia.registrarPuntajeEquipo2( 40 );
        cupiTourColombia.registrarPuntajeEquipo3( 80 );
        cupiTourColombia.registrarPuntajeEquipo4( 200 );
        cupiTourColombia.avanzarEtapa( );
    }

    /**
     * Prueba 1: Se encarga de verificar el m�todo constructor de la clase.<br>
     * <b> M�todos a probar: </b> <br>
     * CupiTourColombia<br>
     * darEtapaActual<br>
     * darEquipo1<br>
     * darEquipo2<br>
     * darEquipo3<br>
     * <b> Caso de prueba:</b><br>
     * 1. Construye correctamente CupiTourColombia, los equipos son creados correctamente.
     */
    @Test
    public void testInicializacion( )
    {
        setupEscenario1( );

        assertEquals( "La etapa actual no es la esperada.", 1, cupiTourColombia.darEtapaActual( ) );

        assertNotNull( "El equipo 1 debe estar inicializado", cupiTourColombia.darEquipo1( ) );
        assertNotNull( "El equipo 2 debe estar inicializado", cupiTourColombia.darEquipo2( ) );
        assertNotNull( "El equipo 3 debe estar inicializado", cupiTourColombia.darEquipo3( ) );
        assertNotNull( "El equipo 3 debe estar inicializado", cupiTourColombia.darEquipo3( ) );

        Equipo equipo1 = cupiTourColombia.darEquipo1( );
        assertEquals( "El c�digo del equipo no es el esperado.", "TNK", equipo1.darCodigo( ) );
        assertEquals( "El nombre del equipo no es el esperado", "Tinkoff", equipo1.darNombre( ) );
        assertEquals( "El a�o de fundaci�n no es el esperado", 2016, equipo1.darAnioFundacion( ) );
        assertEquals( "El pa�s del equipo no es el esperado", "Rusia", equipo1.darPais( ) );
        assertEquals( "El puntaje del equipo no es el esperado", 0, equipo1.darPuntaje( ) );

        Equipo equipo2 = cupiTourColombia.darEquipo2( );
        assertEquals( "El c�digo del equipo no es el esperado.", "SKY", equipo2.darCodigo( ) );
        assertEquals( "El nombre del equipo no es el esperado", "Team Sky", equipo2.darNombre( ) );
        assertEquals( "El a�o de fundaci�n no es el esperado", 2009, equipo2.darAnioFundacion( ) );
        assertEquals( "El pa�s del equipo no es el esperado", "Reino Unido", equipo2.darPais( ) );
        assertEquals( "El puntaje del equipo no es el esperado", 0, equipo2.darPuntaje( ) );

        Equipo equipo3 = cupiTourColombia.darEquipo3( );
        assertEquals( "El c�digo del equipo no es el esperado.", "COF", equipo3.darCodigo( ) );
        assertEquals( "El nombre del equipo no es el esperado", "Cofidis", equipo3.darNombre( ) );
        assertEquals( "El a�o de fundaci�n no es el esperado", 1997, equipo3.darAnioFundacion( ) );
        assertEquals( "El pa�s del equipo no es el esperado", "Francia", equipo3.darPais( ) );
        assertEquals( "El puntaje del equipo no es el esperado", 0, equipo3.darPuntaje( ) );

        Equipo equipo4 = cupiTourColombia.darEquipo4( );
        assertEquals( "El c�digo del equipo no es el esperado.", "MOT", equipo4.darCodigo( ) );
        assertEquals( "El nombre del equipo no es el esperado", "Movistar Team", equipo4.darNombre( ) );
        assertEquals( "El a�o de fundaci�n no es el esperado", 2011, equipo4.darAnioFundacion( ) );
        assertEquals( "El pa�s del equipo no es el esperado", "Colombia", equipo4.darPais( ) );
        assertEquals( "El puntaje del equipo no es el esperado", 0, equipo4.darPuntaje( ) );

    }

    /**
     * Prueba 2: Se encarga de verificar el m�todo avanzarEtapa.<br>
     * <b> M�todos a probar: </b> <br>
     * avanzarEtapa<br>
     * darEtapaActual<br>
     * <b>Casos de prueba: </b> <br>
     * 1. El tour se encuentra en la etapa 1. 2. El tour se encuentra en la etapa 2. 3. El tour se encuentra en la etapa 3.
     */
    @Test
    public void testAvanzarEtapa( )
    {
        setupEscenario1( );

        // Caso de prueba 1
        cupiTourColombia.avanzarEtapa( );
        assertEquals( "La etapa actual no es la esperada.", 2, cupiTourColombia.darEtapaActual( ) );

        // Caso de prueba 2
        cupiTourColombia.avanzarEtapa( );
        assertEquals( "La etapa actual no es la esperada.", 3, cupiTourColombia.darEtapaActual( ) );

        // Caso de prueba 3
        cupiTourColombia.avanzarEtapa( );
        assertEquals( "La etapa actual no es la esperada.", 4, cupiTourColombia.darEtapaActual( ) );
    }

    /**
     * Prueba 3: Se encarga de verificar el m�todo registrarPuntajeEquipo1.<br>
     * <b> M�todos a probar: </b> <br>
     * registrarPuntajeEquipo1<br>
     * darEquipo1<br>
     * darPuntaje<br>
     * <b> Caso de prueba:</b><br>
     * 1. Se registra un puntaje nuevo al equipo 1.<br>
     * 2. Se registra un puntaje nuevo al equipo 2. <br>
     * 3. Se registra un puntaje nuevo al equipo 3. <br>
     * 4. Se registra un puntaje nuevo al equipo 4.
     */
    @Test
    public void testRegistrarPuntaje( )
    {
        setupEscenario1( );

        // Caso de prueba 1
        cupiTourColombia.registrarPuntajeEquipo1( 100 );
        assertEquals( "No se registr� correctamente el puntaje, el puntaje total del equipo no corresponde al esperado.", 100, cupiTourColombia.darEquipo1( ).darPuntaje( ) );

        cupiTourColombia.registrarPuntajeEquipo1( 35 );
        assertEquals( "No se registr� correctamente el puntaje, el puntaje total del equipo no corresponde al esperado.", 135, cupiTourColombia.darEquipo1( ).darPuntaje( ) );

        // Caso de prueba 2
        cupiTourColombia.registrarPuntajeEquipo2( 100 );
        assertEquals( "No se registr� correctamente el puntaje, el puntaje total del equipo no corresponde al esperado.", 100, cupiTourColombia.darEquipo2( ).darPuntaje( ) );

        cupiTourColombia.registrarPuntajeEquipo2( 35 );
        assertEquals( "No se registr� correctamente el puntaje, el puntaje total del equipo no corresponde al esperado.", 135, cupiTourColombia.darEquipo2( ).darPuntaje( ) );

        // Caso de prueba 3
        cupiTourColombia.registrarPuntajeEquipo3( 100 );
        assertEquals( "No se registr� correctamente el puntaje, el puntaje total del equipo no corresponde al esperado.", 100, cupiTourColombia.darEquipo3( ).darPuntaje( ) );

        cupiTourColombia.registrarPuntajeEquipo3( 35 );
        assertEquals( "No se registr� correctamente el puntaje, el puntaje total del equipo no corresponde al esperado.", 135, cupiTourColombia.darEquipo3( ).darPuntaje( ) );

        // Caso de prueba 4
        cupiTourColombia.registrarPuntajeEquipo4( 100 );
        assertEquals( "No se registr� correctamente el puntaje, el puntaje total del equipo no corresponde al esperado.", 100, cupiTourColombia.darEquipo4( ).darPuntaje( ) );

        cupiTourColombia.registrarPuntajeEquipo4( 35 );
        assertEquals( "No se registr� correctamente el puntaje, el puntaje total del equipo no corresponde al esperado.", 135, cupiTourColombia.darEquipo4( ).darPuntaje( ) );
    }

    /**
     * Prueba 4: Se encarga de verificar el m�todo registrarPenalizacionEquipo1.<br>
     * <b> M�todos a probar: </b> <br>
     * registrarPenalizacionEquipo1<br>
     * darEquipo1<br>
     * darPuntaje<br>
     * <b> Caso de prueba:</b><br>
     * 1. Se registra una nueva penalizaci�n al equipo 1.<br>
     * 2. Se registra una nueva penalizaci�n al equipo 2.<br>
     * 3. Se registra una nueva penalizaci�n al equipo 3.<br>
     * 4. Se registra una nueva penalizaci�n al equipo 4.<br>
     */
    @Test
    public void testRegistrarPenalizacion( )
    {
        setupEscenario2( );

        // Caso de prueba 1
        cupiTourColombia.registrarPenalizacionEquipo1( 30 );
        assertEquals( "No se registr� correctamente la penalizaci�n, el puntaje total del equipo no corresponde al esperado.", 130, cupiTourColombia.darEquipo1( ).darPuntaje( ) );

        cupiTourColombia.registrarPenalizacionEquipo1( 10 );
        assertEquals( "No se registr� correctamente la penalizaci�n, el puntaje total del equipo no corresponde al esperado.", 120, cupiTourColombia.darEquipo1( ).darPuntaje( ) );

        // Caso de prueba 2
        cupiTourColombia.registrarPenalizacionEquipo2( 30 );
        assertEquals( "No se registr� correctamente la penalizaci�n, el puntaje total del equipo no corresponde al esperado.", 130, cupiTourColombia.darEquipo2( ).darPuntaje( ) );

        cupiTourColombia.registrarPenalizacionEquipo2( 10 );
        assertEquals( "No se registr� correctamente la penalizaci�n, el puntaje total del equipo no corresponde al esperado.", 120, cupiTourColombia.darEquipo2( ).darPuntaje( ) );

        // Caso de prueba 3
        cupiTourColombia.registrarPenalizacionEquipo3( 30 );
        assertEquals( "No se registr� correctamente la penalizaci�n, el puntaje total del equipo no corresponde al esperado.", 100, cupiTourColombia.darEquipo3( ).darPuntaje( ) );

        cupiTourColombia.registrarPenalizacionEquipo3( 10 );
        assertEquals( "No se registr� correctamente la penalizaci�n, el puntaje total del equipo no corresponde al esperado.", 90, cupiTourColombia.darEquipo3( ).darPuntaje( ) );

        // Caso de prueba 4
        cupiTourColombia.registrarPenalizacionEquipo4( 30 );
        assertEquals( "No se registr� correctamente la penalizaci�n, el puntaje total del equipo no corresponde al esperado.", 280, cupiTourColombia.darEquipo4( ).darPuntaje( ) );

        cupiTourColombia.registrarPenalizacionEquipo4( 10 );
        assertEquals( "No se registr� correctamente la penalizaci�n, el puntaje total del equipo no corresponde al esperado.", 270, cupiTourColombia.darEquipo4( ).darPuntaje( ) );
    }

    /**
     * Prueba 5: Se encarga de verificar el m�todo calcularPuntajeTotal.<br>
     * <b> M�todos a probar: </b> <br>
     * calcularPuntajeTotal<br>
     * registrarPuntajeEquipo1<br>
     * registrarPuntajeEquipo2<br>
     * registrarPuntajeEquipo3<br>
     * registrarPuntajeEquipo4<br>
     * <b> Caso de prueba:</b><br>
     * 1. Ning�n equipo tiene puntos.<br>
     * 2. Dos equipos tienen puntos.<br>
     * 3. Cuatro equipos tienen puntos.<br>
     */
    @Test
    public void testCalcularPuntajeTotal( )
    {
        setupEscenario1( );

        // Caso de prueba 1
        assertEquals( "El puntaje total no corresponde.", 0, cupiTourColombia.calcularPuntajeTotal( ) );

        // Caso de prueba 2
        cupiTourColombia.registrarPuntajeEquipo1( 10 );
        cupiTourColombia.registrarPuntajeEquipo4( 50 );
        assertEquals( "El puntaje total no corresponde.", 60, cupiTourColombia.calcularPuntajeTotal( ) );

        // Caso de prueba 3
        cupiTourColombia.registrarPuntajeEquipo1( 100 );
        cupiTourColombia.registrarPuntajeEquipo4( 450 );
        assertEquals( "El puntaje total no corresponde.", 610, cupiTourColombia.calcularPuntajeTotal( ) );

    }

    /**
     * Prueba 6: Se encarga de verificar el m�todo calcularPuntajePromedio.<br>
     * <b> M�todos a probar: </b> <br>
     * calcularPuntajePromedio.<br>
     * registrarPuntajeEquipo1<br>
     * registrarPuntajeEquipo2<br>
     * registrarPuntajeEquipo3<br>
     * registrarPuntajeEquipo4<br>
     * <b> Caso de prueba:</b><br>
     * 1. Ning�n equipo tiene puntaje.<br>
     * 2. Dos equipos tienen puntaje.<br>
     * 3. Cuatro equipos tienen puntaje.<br>
     */
    @Test
    public void testCalcularPuntajePromedio( )
    {
        setupEscenario1( );

        // Caso de prueba 1
        assertEquals( "El puntaje promedio no corresponde.", 0, cupiTourColombia.calcularPuntajePromedio( ), 0.1 );

        // Caso de prueba 2
        cupiTourColombia.registrarPuntajeEquipo2( 25 );
        cupiTourColombia.registrarPuntajeEquipo3( 100 );
        assertEquals( "El puntaje promedio no corresponde.", 31.25, cupiTourColombia.calcularPuntajePromedio( ), 0.01 );

        // Caso de prueba 3
        cupiTourColombia.registrarPuntajeEquipo1( 16 );
        cupiTourColombia.registrarPuntajeEquipo2( 42 );
        cupiTourColombia.registrarPuntajeEquipo3( 2 );
        cupiTourColombia.registrarPuntajeEquipo4( 15 );

        assertEquals( "El puntaje promedio no corresponde.", 50.0, cupiTourColombia.calcularPuntajePromedio( ), 0.1 );

    }
    /**
     * Prueba 7: Se encarga de verificar el m�todo calcularPuntajePromedioPorEtapa.<br>
     * <b> M�todos a probar: </b> <br>
     * calcularPuntajePromedioPorEtapa.<br>
     * registrarPuntajeEquipo1<br>
     * registrarPuntajeEquipo2<br>
     * registrarPuntajeEquipo3<br>
     * registrarPuntajeEquipo4<br>
     * <b> Caso de prueba:</b><br>
     * 1. Ning�n equipo tiene puntaje y la etapa actual es 0.<br>
     * 2. Dos equipos tienen puntaje y la etapa actual es 2.<br>
     * 3. Cuatro equipos tienen puntaje y la etapa actual es 4.<br>
     */
    @Test
    public void testCalcularPuntajePromedioPorEtapa( )
    {
        setupEscenario1( );

        // Caso de prueba 1
        assertEquals( "El puntaje promedio no corresponde.", 0, cupiTourColombia.calcularPuntajePromedioPorEtapa( ), 0.1 );

        // Caso de prueba 2
        cupiTourColombia.avanzarEtapa( );
        cupiTourColombia.registrarPuntajeEquipo1( 5 );
        cupiTourColombia.registrarPuntajeEquipo3( 100 );
        assertEquals( "El puntaje promedio no corresponde.", 13.125, cupiTourColombia.calcularPuntajePromedioPorEtapa( ), 0.01 );

        // Caso de prueba 3
        cupiTourColombia.avanzarEtapa( );
        cupiTourColombia.avanzarEtapa( );
        cupiTourColombia.registrarPuntajeEquipo2( 50 );
        cupiTourColombia.registrarPuntajeEquipo3( 4 );
        cupiTourColombia.registrarPuntajeEquipo4( 13 );

        assertEquals( "El puntaje promedio no corresponde.", 10.75, cupiTourColombia.calcularPuntajePromedioPorEtapa( ), 0.1 );
    }
    /**
     * Prueba 8: Se encarga de verificar el m�todo calcularPorcentajeEtapasRealizadas.<br>
     * <b> M�todos a probar: </b> <br>
     * calcularPorcentajeEtapasRealizadas.<br>
     * registrarPuntajeEquipo1<br>
     * registrarPuntajeEquipo2<br>
     * registrarPuntajeEquipo3<br>
     * registrarPuntajeEquipo4<br>
     * <b> Caso de prueba:</b><br>
     * 1. El tour se encuentra en la etapa 1.<br>
     * 2. El tour se encuentra en la etapa 3.<br>
     * 3. El tour ya finaliz�.<br>
     */
    @Test
    public void testCalcularPorcentajeEtapasRealizadas( )
    {
        setupEscenario1( );

        // Caso de prueba 1
        assertEquals( "El porcentaje no corresponde.", 0, cupiTourColombia.calcularPorcentajeEtapasTerminadas( ), 0.1 );

        // Caso de prueba 2
        cupiTourColombia.avanzarEtapa( );
        cupiTourColombia.avanzarEtapa( );
        assertEquals( "El porcentaje no corresponde.", 40, cupiTourColombia.calcularPorcentajeEtapasTerminadas( ), 0.01 );

        // Caso de prueba 3
        cupiTourColombia.avanzarEtapa( );
        cupiTourColombia.avanzarEtapa( );
        cupiTourColombia.avanzarEtapa( );
        assertEquals( "El porcentaje no corresponde.", 100, cupiTourColombia.calcularPorcentajeEtapasTerminadas( ), 0.01 );
    }

    /**
     * Prueba 9: Se encarga de verificar el m�todo reiniciar.<br>
     * <b> M�todos a probar: </b> <br>
     * reiniciar<br>
     * darPuntaje<br>
     * darEtapa<br>
     * <b> Caso de prueba:</b><br>
     * 1. El equipo tiene puntaje y la etapa actual es la etapa 3.<br>
     * 2. El equipo no tiene puntaje y la etapa actual es la etapa 0.
     */
    @Test
    public void testReiniciar( )
    {
        setupEscenario2( );

        //Caso de prueba 1
        cupiTourColombia.reiniciar( );
        assertEquals( "No reinici� correctamente, el puntaje del equipo 1 no corresponde.", 0, cupiTourColombia.darEquipo1( ).darPuntaje( ) );
        assertEquals( "No reinici� correctamente, el puntaje del equipo 2 no corresponde.", 0, cupiTourColombia.darEquipo2( ).darPuntaje( ) );
        assertEquals( "No reinici� correctamente, el puntaje del equipo 3 no corresponde.", 0, cupiTourColombia.darEquipo3( ).darPuntaje( ) );
        assertEquals( "No reinici� correctamente, el puntaje del equipo 4 no corresponde.", 0, cupiTourColombia.darEquipo4( ).darPuntaje( ) );
        assertEquals( "No reinici� correctamente, la etapa no es la esperada.", 1, cupiTourColombia.darEtapaActual( ) );

        //Caso de prueba 2
        cupiTourColombia.reiniciar( );
        assertEquals( "No reinici� correctamente, el puntaje del equipo 1 no corresponde.", 0, cupiTourColombia.darEquipo1( ).darPuntaje( ) );
        assertEquals( "No reinici� correctamente, el puntaje del equipo 2 no corresponde.", 0, cupiTourColombia.darEquipo2( ).darPuntaje( ) );
        assertEquals( "No reinici� correctamente, el puntaje del equipo 3 no corresponde.", 0, cupiTourColombia.darEquipo3( ).darPuntaje( ) );
        assertEquals( "No reinici� correctamente, el puntaje del equipo 4 no corresponde.", 0, cupiTourColombia.darEquipo4( ).darPuntaje( ) );
        assertEquals( "No reinici� correctamente, la etapa no es la esperada.", 1, cupiTourColombia.darEtapaActual( ) );

    }

}